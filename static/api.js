var activeObject = 1;
$(document).ready(function() {
  var apiobjget    = "http://localhost:4005/object/api/object/get/"
  var apiobjlist   = "http://localhost:4005/object/api/get/objectlist";
  var apifilelist  = "http://localhost:4005/object/api/get/template/list";
  apichilddata = "http://localhost:4005/object/api/object/get/childs/from/" + activeObject ;
  $.getJSON(apiobjlist, function(emp) {
    for (var i in emp)
    {
      $('#objlist').append('<option value='+emp[i].id+'>'+ emp[i].id + ' ' + emp[i].name +  '</p>');
      $('#parentlist').append('<option value='+emp[i].id+'>'+ emp[i].id + ' ' + emp[i].name +  '</p>');
      $('#childlist').append('<option value='+emp[i].id+'>'+ emp[i].id + ' ' + emp[i].name +  '</p>');
    }
  });

  $.getJSON(apifilelist, function(emp) {
    for (var i in emp)
    {
      $('#filelist').append('<option value=' + emp[i] + '>'+ emp[i] + '</p>');
    }
  });
  selectedObject = $("div#aojb").html();
  // getObjectData(apiobjget + selectedObject);
  getApiChildData(apichilddata);
  // getChildData(apichilddata);

  $(".overviewimage").click(function(){
    console.log("toggle click");
    $("#overviewimage").dropDown();
  });

  $(".addParent").click(function(){
    var activeObject = $("#aobj").html();
    var selectedObject = $("#parentlist").children("option:selected").val();
    $.ajax({
      url: 'http://localhost:4005/object/api/object/add/parent/' + activeObject + '/' + selectedObject,
      type: 'get'
    })
    .done(function() {
      $('#status').slideDown(500);
      $('#status').html("Done");
      setTimeout(function(){ $('#status').slideUp(500); }, 3000);
    })
    .fail(function() {
      $('#status').show();
      $('#status').html("error")
    })
    .always(function() {
      $('#status').slideDown(500);
      $('#status').html("Add parent complete")
      getParentData()
      setTimeout(function(){ $('#status').slideUp(500); }, 3000);
    });
  });

  $(".addChild").click(function(){
    var activeObject = $("#aobj").html();
    var selectedObject = $("#childlist").children("option:selected").val();
    $.ajax({
      url: 'http://localhost:4005/object/api/object/add/child/' + activeObject + '/' + selectedObject,
      type: 'get'
    })
    .done(function() {
      $('#status').slideDown(500);
      $('#status').html("Done");
      setTimeout(function(){ $('#status').slideUp(500); }, 3000);
    })
    .fail(function() {
      $('#status').show();
      $('#status').html("error")
    })
    .always(function() {
      $('#status').slideDown(500);
      $('#status').html("Add Child complete. ");
      getApiChildData(apichilddata);
      getChildData(apichilddata);
      setTimeout(function(){ $('#status').slideUp(500); }, 3000);
    });
  });
  $("select#objlist").change(function(){
    var selectedObject = $(this).children("option:selected").val();
    var url = 'http://localhost:4005/object/api/object/get/';
    $( "div#aobj" ).html(selectedObject);
    getObjectData(url+selectedObject);
    activeObject = selectedObject;
    getChildData(apichilddata);
    getParentData();
  });
});
/* End on ready */

$(document).ready("#rch").on('click','.item',function(){
  var activeObject = $("#aobj").html();
  var classes = $(this).parent().attr('class');
  var selectedObject = $("input#"+classes).val();
  console.log("activeObject " + activeObject + " classes " + classes + " selectedObject " + selectedObject);
  var url = "/object/api/object/remove/child/" + activeObject + "/" + selectedObject;
  $.ajax({
    url: 'http://localhost:4005'+ url,
    type: 'get'
  })
  .done(function() {
    $('#status').slideDown(500);
    $('#status').html("Done");
    setTimeout( function(){ $('#status').slideUp(500); }, 3000);
  })
  .fail(function() {
    $('#status').show();
    $('#status').html("error")
  })
  .always(function() {
    $('#status').slideDown(500);
    $('#status').html("Delete child complete");
    getApiChildData(apichilddata);
    getChildData(apichilddata);
    setTimeout( function(){ $('#status').slideUp(500); }, 3000);
  });
});
$(document).ready("pdata > button#rpa").on('click','.pitem',function(){
  var activeObject = $("#aobj").html();
  var classes = $(this).parent().attr('class');
  var selectedObject = $("input#"+classes).val();
  var url = "/object/api/object/remove/parent/" + activeObject + "/" + selectedObject;
  $.ajax({
    url: 'http://localhost:4005'+ url,
    type: 'get'
  })
  .done(function() {
    $('#status').slideDown(500);
    $('#status').html("Done");
    setTimeout(function(){ $('#status').slideUp(500); }, 3000);
  })
  .fail(function() {
    $('#status').show();
    $('#status').html("error")
  })
  .always(function() {
    $('#status').slideDown(500);
    $('#status').html("Delete parent complete")
    getParentData()
    setTimeout( function(){ $('#status').slideUp(500); }, 3000);
  });
});
function getChild(url) {
  $.getJSON(url, function(data) {
      myItems = data.items;
      var activeObject = $("#aobj").html();
      for (var i in data) {
        if ( i !== "childs" && i !== "parent" ){
          $('#cdata').append('<p class="'+ i + data['id']+'"><b>'+i+':</b> ' + data[i] + '</p>');
        }
      }
      $('#cdata').append('<div class="cdelete'+data['id']+'"><input id="cdelete'+data['id']+'" type="hidden" name="obj_id" value="' + data['id'] + '"><button id="rpa" class="item">Löschen</button></div></form><hr>');
  });
}
function getChildData(apichilddata) {
  var url = 'http://localhost:4005/object/api/object/get/childs/from/';
  var selectedObject = $("#aobj").html();
  $.getJSON(url + selectedObject , function(data) {
      myItems = data.items;
      $( "#cdata" ).empty();
      const ordered = {};
      Object.keys(data).sort().forEach(function(key) {
        ordered[key] = data[key];
      });
      for (var i in ordered) {
        getChild("http://localhost:4005/object/api/object/get/" + i)
      }
  });
}

function getParent(url) {
  $.getJSON(url, function(pdata) {
      myItems = pdata.items;
      var activeObject = $("#aobj").html();
      for (var i in pdata) {
        if ( i !== "childs" && i !== "parent" ){
          $('#pdata').append('<p class="'+ i + pdata['id']+'"><b>'+i+':</b> ' + pdata[i] + '</p>');
        }
      }
      $('#pdata').append('<div class="pdelete'+pdata['id']+'"><input id="pdelete'+pdata['id']+'" type="hidden" name="obj_id" value="' + pdata['id'] + '"><button id="rpa" class="pitem">Löschen</button></div></form><hr>');
  });
}
function getParentData() {
  var url = 'http://localhost:4005/object/api/object/get/parent/from/';
  var selectedObject = $("#aobj").html();
  $.getJSON(url + selectedObject , function(data) {
      myItems = data.items;
      $( "#pdata" ).empty();
      const ordered = {};
      Object.keys(data).sort().forEach(function(key) {
        ordered[key] = data[key];
      });
      for (var i in ordered) {
        getParent("http://localhost:4005/object/api/object/get/" + i);
      }
  });
}

function getObjectData(apiobjget){
  var selectedObject = $("#aobj").html();
  $.getJSON("http://localhost:4005/object/api/object/get/" + selectedObject, function(emp) {
    $( "#objdata" ).empty();
    for (var i in emp)
    {
      if ( i !== "childs" && i !== "parent" ){ $('#objdata').append('<p><b>' + i + '</b>: '+ emp[i] + '</p>'); }
    }
  });
}
function getApiChildData(apichilddata) {
  var selectedObject = $("#aobj").html();
  $.getJSON("http://localhost:4005/object/api/object/get/childs/from/" + selectedObject , function(data) {
      $( "#childdata" ).empty();
      myItems = data.items;
      const ordered = {};
      Object.keys(data).sort().forEach(function(key) { ordered[key] = data[key]; });
      for (var i in ordered) {
        $('#childdata').append('<p><a href="http://localhost:4005/object/'+ i + '">('+i+') ' + ordered[i] + '</a></p>');
      }
  })
  .done(function()   { /* console.log( "second success" ); */ })
  .fail(function()   { /* console.log( "error" ); */ })
  .always(function() { /* console.log( "complete" ); */ });
}
