from flask import render_template, request, session, redirect, url_for
# from flask import Flask, current_app
from flask_jsonpify import jsonify
from flask import Flask, flash, redirect, render_template, request, session, abort, current_app
from flask_restful import Resource, Api
# from sqlalchemy.orm import sessionmaker
# from sqlalchemy import create_engine
from flask_cors import CORS
# from api.users import *
from libs.usess import *
from libs.srvtree import *
import sys
import json
import os
import logging
import requests
from datetime import datetime
from flasgger import Swagger
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.config["SECRET_KEY"] = "OCML3BRawWEUeaxcuKHLpw"

users = json.loads(open("users.json",'r').read())
data = json.loads(open("data.json",'r').read())
s = usess(users,data)
st = srvtree()

@app.route("/profile")
def profile():
    if not session.get("USERNAME") is None:
        username = session.get("USERNAME")
        user = users[username]
        return render_template("profile.html", user=user, MAIL=session.get("MAIL"))
    else:
        print("No username found in session")
        return redirect(url_for("sign_in"))

@app.route("/sign-out")
def sign_out():
    (r, f) = s.s_sign_out()
    return redirect(url_for("sign_in"))

@app.route("/sign-in", methods=["GET", "POST"])
def sign_in():
    service = request.args.get('service', default = '*', type = str)
    print ("Service: "+ service)
    (r , f) = s.s_sign_in(request)
    if r == "t":
        return render_template(f, service=service)
    elif r == "r":
        if service == "/object/1":
            redirect(service)
        return redirect("/sign-in")
    else:
        if service == "/object/1":
            print ("taddaaaa")
            redirect(service)
        return redirect(url_for("profile"))

@app.route("/get/uuid", methods=["GET"])
def get_uuid():
    username = session.get("USERNAME")
    user = users[username]
    uuid = s.gen_uuid()
    print ("uuid: " + uuid )
    return render_template("profile.html", user=user, uuid=uuid)

@app.route("/object/addparent", methods=["GET","POST"])
def addparent():
    if not session.get("USERNAME") is None:
        username = session.get("USERNAME")
        user = users[username]
        id = s.addparent(request)
        data = s.get_object(id)
        obj_list = s.get_objectlist()
        return redirect("/object/" + id )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/addchild", methods=["GET","POST"])
def addchild():
    if not session.get("USERNAME") is None:
        id = s.addchild(request)
        data = s.get_object(id)
        obj_list = s.get_objectlist()
        return redirect("/object/" + id )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/delparent/<id>/<parent>", methods=["GET","POST"])
def delparent(id,parent):
    if not session.get("USERNAME") is None:
        s.del_parent(id,parent)
        return redirect("/object/" + id )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/delchild/<id>/<child>", methods=["GET","POST"])
def delchild(id,child):
    if not session.get("USERNAME") is None:
        s.del_child(id,child)
        return redirect("/object/" + id )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/redirect", methods=["GET","POST"])
def redirectto():
    req = request.form
    id = req.get("id")
    print ("id:", id)
    return redirect("/object/" + id )

@app.route("/object/delete/<id>", methods=["GET","POST"])
def delete(id):
    if not session.get("USERNAME") is None:
        s.del_object(id)
        return redirect("/object/9b615ce4-f78e-11e9-9d1a-8c8590c2b1ec" )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/data/test.json", methods=["GET"])
def test_json():
    return render_template('test.json', id=id )

@app.route("/object/", methods=["GET"])
def get_object_site():
    if not session.get("USERNAME") is None:
        username = session.get("USERNAME")
        user = users[username]
        id = s.api_get_first_id()
        return redirect("/object/" + id )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/<id>", methods=["GET"])
def get_object(id):
    if not session.get("USERNAME") is None:
        username = session.get("USERNAME")
        user = users[username]
        data = s.get_object(id)
        # obj_list = s.get_objectlist()
        clist = s.get_childlist(id)
        plist = s.get_parentlist(id)
        # cjson = s.get_childs_data(clist)
        # pjson = s.get_parent_data(plist)
        # tpllist = s.read_tpl_list()
        st.draw(id,plist,clist)

        # return render_template('object.html', id=id, data=data, user=user, obj_list=obj_list, cjson=cjson, pjson=pjson, tpllist=tpllist )
        return render_template('object.html', id=id, data=data, user=user )
    else:
        print("No username found in session")
        return redirect("/log-in")

# @app.route("/object/list", methods=["GET"])
# def get_objectlist():
#     obj_list = s.get_objectlist()
#     return render_template('object.html', obj_list=obj_list)

@app.route("/object/add", methods=["POST"])
def add_object():
    if not session.get("USERNAME") is None:
        (state , text, uuid) = s.add_object(request)
        data = s.get_object(uuid)
        return redirect("/object/" + uuid )
    else:
        print("No username found in session")
        return redirect("/log-in")

### test
@app.route("/object/api/object/add", methods=["POST"])
def api_add_object():
    # if not session.get("USERNAME") is None:
    state = s.api_add_object(request)
    # print ("uuid: " + str(state['result']['uuid']))
    # data = s.get_object(uuid)
    return state
    # else:
    #     print("No username found in session")
    #     return redirect("/log-in")


@app.route("/object/addfield/", methods=["POST"])
def add_object_field():
    if not session.get("USERNAME") is None:
        (state , text, id) = s.add_object_field(request)
        return redirect("/object/" + str(id) )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/templates/", methods=["GET"])
def templates():
    if not session.get("USERNAME") is None:
        tpl = s.read_tpl_list()
        print ("tpl list:" , tpl)
        return render_template("templates.html", tpl=tpl)
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/template/get/<id>", methods=["GET"])
def get_template(id):
    if not session.get("USERNAME") is None:
        username = session.get("USERNAME")
        user = users[username]
        data = s.get_template(id)
        tpl = s.read_tpl_list()
        return render_template('templates.html', tpl=tpl, data=data, user=user, id=id )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/template/add", methods=["POST"])
def add_template():
    if not session.get("USERNAME") is None:
        (state , text ) = s.add_template(request)
        return redirect("/object/templates/" )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/template/addfield", methods=["POST"])
def add_template_field():
    if not session.get("USERNAME") is None:
        (state , text, id) = s.add_template_field(request)
        return redirect("/object/template/get/" + str(id) )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/template/delfield/<id>/<k>", methods=["GET"])
def del_template_field(id,k):
    if not session.get("USERNAME") is None:
        print ("id:" + id + " k: " + k)
        (state , text, id) = s.del_template_field(id,k)
        return redirect("/object/template/get/" + str(id) )
    else:
        print("No username found in session")
        return redirect("/log-in")

########################################################
#
# API
#

@app.route("/object/api/object/get/<id>", methods=["GET"])
def api_object_get(id):
    data = s.get_object(id)
    return json.dumps(data)

@app.route("/object/api/get/objectlist", methods=["GET"])
def api_get_objlist():
    obj_list = s.api_objectlist()
    return json.dumps(obj_list)

@app.route("/object/api/get/template/list", methods=["GET"])
def api_template_list():
    obj_list = s.api_tpl_list()
    return json.dumps(obj_list)
@app.route("/object/api/object/get/childs/from/<id>", methods=["GET"])
def api_object_get_childs_from(id):
    data = s.api_get_childs_from_object(id)
    return json.dumps(data)

@app.route("/object/api/object/get/parent/from/<id>", methods=["GET"])
def api_get_parents_from_object(id):
    data = s.api_get_parents_from_object(id)
    return json.dumps(data)

@app.route("/object/api/object/add/parent/<id>/<parent>", methods=["GET"])
def api_add_parent(id,parent):
    if not session.get("USERNAME") is None:
        id = s.api_add_parent(id,parent)
        return redirect("/object/" + id )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/api/object/add/child/<id>/<child>", methods=["GET"])
def api_add_child(id,child):
    if not session.get("USERNAME") is None:
        state = s.api_add_child(id,child)
        return json.dumps(state)
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/api/object/add/field/", methods=["POST"])
def api_add_object_field():
    if not session.get("USERNAME") is None:
        (state , text, id) = s.api_add_object_field(request)
        return state
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/api/object/remove/parent/<id>/<parent>", methods=["GET","POST"])
def api_del_parent(id,parent):
    if not session.get("USERNAME") is None:
        s.api_del_parent(id,parent)
        return redirect("/object/" + id )
    else:
        print("No username found in session")
        return redirect("/log-in")

@app.route("/object/api/object/remove/child/<id>/<child>", methods=["GET","POST"])
def api_delchild(id,child):
    if not session.get("USERNAME") is None:
        s.api_del_child(id,child)
        return redirect("/object/" + id )
    else:
        print("No username found in session")
        return redirect("/log-in")

if __name__ == "__main__":
  # app.secret_key = os.urandom(12)
  # app.wsgi_app = ConsoleLog(app.wsgi_app, console)
  app.run(debug=True,host='0.0.0.0', port=4005)
