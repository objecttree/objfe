FROM base
COPY ./ /app
WORKDIR /app

EXPOSE 4005
CMD python ./app.py
