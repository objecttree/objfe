import networkx as nx
import matplotlib.pyplot as plt
import json

class srvtree():

    def __init__(self):
      self.did      = ""

    def read_data(self):
        return json.loads(open('data.json','r').read())

    def draw(self,id,parents,childs):
        data = self.read_data()
        plt.switch_backend('Agg')
        G=nx.Graph()
        obj_id = data[id]['id']
        obj_name = data[id]['name']
        G.add_edge(str(obj_id) + " " + str(obj_name),"Parents",weight=0.1)
        G.add_edge(str(obj_id) + " " + str(obj_name),"Childs",weight=0.6)
        for pid in parents:
            pname = data[pid]['name']
            G.add_edge("Parents",str(pid) + " " + str(pname), weigth=0.6)

        for cid in childs:
            cname = data[cid]['name']
            G.add_edge("Childs",str(cid) + " " + str(cname), weigth=0.6)

        elarge=[(u,v) for (u,v,d) in G.edges(data=True)]# if d['weight'] >0.5]
        # esmall=[(u,v) for (u,v,d) in G.edges(data=True) if d['weight'] <=0.5]

        pos=nx.spring_layout(G)
        nx.draw_networkx_nodes(G,pos,node_size=1000)
        nx.draw_networkx_edges(G,pos,node_size=30,edgelist=elarge, width=1, edge_color='g')
        # nx.draw_networkx_edges(G,pos,edgelist=esmall, width=1,alpha=0.5,edge_color='g',style='dashed')
        nx.draw_networkx_labels(G,pos,font_size=5,font_family='verdana')
        plt.savefig("static/"+str(id)+".png")
