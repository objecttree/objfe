from flask import render_template, request, session, redirect, url_for
from flask_jsonpify import jsonify
from flask import Flask, flash, redirect, render_template, request, session, abort, current_app
from flask_restful import Resource, Api
from flask_cors import CORS
import json
import uuid
import os
from werkzeug.utils import secure_filename
from jsonmerge import merge

class usess():

    def __init__(self,users,data):
      self.users = users
      self.data  = data

    def read_data(self):
        return json.loads(open('data.json','r').read())

    def write_data(self,data):
        with open("data.json", "w") as twitter_data_file:
            json.dump(data, twitter_data_file, indent=4, sort_keys=True)

    def read_tpl(self,tpl):
        print ("read_tpl: tpl:", tpl)
        return json.loads(open('tpl/' + tpl,'r').read())

    def write_tpl(self,tpl,data):
        with open('tpl/' + tpl, "w") as twitter_data_file:
            json.dump(data, twitter_data_file, indent=4, sort_keys=True)

    def read_tpl_list(self):
        path = "tpl"
        files = []
        for r, d, f in os.walk(path):
            for file in f:
                files.append(file)
        return files

    def api_tpl_list(self):
        path = "tpl"
        files = []
        for r, d, f in os.walk(path):
            for file in f:
                files.append(file)
        return files

# get_object_get_childs_from
    def api_get_childs_from_object(self,id):
        data = self.read_data()
        obj = data[id]['childs']
        print ("das?:", obj)
        for key in obj:
            c_data = self.get_object(key)
            obj[key]= c_data['name']
        return obj

    def api_get_parents_from_object(self,id):
        data = self.read_data()
        obj = data[id]['parent']
        print ("das?:", obj)
        for key in obj:
            c_data = self.get_object(key)
            obj[key]= c_data['name']
        return obj

    def get_object(self,id):
        c=0
        data = self.read_data()
        obj = data[id]
        return obj

    def api_objectlist(self):
        data = self.read_data()
        # obj_list = self.read_tpl("temp.json")
        obj_list = {}
        print ("OBJ_LIST: ", obj_list)
        for key in data:
            xlen = len(obj_list)
            one = data[key]
            xlen = int(xlen) + int(1)
            obj_list[xlen] = one
        return obj_list

    # def get_objectlist(self):
    #     obj_list = {}
    #     data = self.read_data()
    #     for key in data:
    #         name = data[key]['name']
    #         id = data[key]['id']
    #         j = { "id": id, "name": name}
    #         obj_list[id] = { "id": id, "name": name}
    #     return obj_list

    def api_add_object_field(self,request):
        req   = request.form
        id    = req.get("id")
        name  = req.get("name")
        value = req.get("value")
        # uuid= self.gen_uuid()
        print (id, name, value)
        data = self.read_data()
        data[id][name] = value
        self.write_data(data)
        return (1, "Object created", id)

    def api_add_child(self,id,child):
        data = self.read_data()
        data[id]['childs'][child] = ""
        self.write_data(data)
        self.connectChildParent(id,child)
        return { "result": "ok" }

    def api_del_child(self,id,child):
        data = self.read_data()
        del data[id]['childs'][child]
        self.write_data(data)
        self.DisconnectChildParent(child,id)
        return

    # def get_child(self,id):
    #     data = self.read_data()
    #     obj = data[id]
    #     return obj

    def get_parentlist(self,id):
        data = self.read_data()
        obj = data[id]['parent']
        return obj

    def get_parent_data(self,list):
        data = self.read_data()
        obj = { "list": [] }
        for id in list:
            obj['list'].append(data[id])
        return obj

    def get_childlist(self,id):
        data = self.read_data()
        obj = data[id]['childs']
        return obj

    # def get_childs_data(self,list):
    #     data = self.read_data()
    #     obj = { "list": [] }
    #     for id in list:
    #         obj['list'].append(data[id])
    #     return obj

    def addparent(self,request):
        data = self.read_data()
        req = request.form
        id = req.get("obj_id")
        parent = req.get("parent_obj")
        data[id]['parent'][parent] = ""
        self.write_data(data)
        #with open("data.json", "w") as twitter_data_file:
        #     json.dump(data, twitter_data_file, indent=4, sort_keys=True)
        self.connectParentChild(id,parent)
        return id

    def api_add_parent(self,id,parent):
        data = self.read_data()
        data[id]['parent'][parent] = ""
        self.write_data(data)
        self.connectParentChild(id,parent)
        return id

    def connectParentChild(self,id,parent):
        data = self.read_data()
        data[parent]['childs'][id] = ""
        self.write_data(data)
        # with open("data.json", "w") as twitter_data_file:
        #    json.dump(data, twitter_data_file, indent=4, sort_keys=True)
        return

    def addchild(self,request):
        data = self.read_data()
        req = request.form
        id = req.get("obj_id")
        child = req.get("child_obj")
        data[id]['childs'][child] = ""
        self.write_data(data)
        # with open("data.json", "w") as twitter_data_file:
        #    json.dump(data, twitter_data_file, indent=4, sort_keys=True)
        self.connectChildParent(id,child)
        return id

    def connectChildParent(self,id,child):
        data = self.read_data()
        data[child]['parent'][id] = ""
        self.write_data(data)
        # with open("data.json", "w") as twitter_data_file:
        #     json.dump(data, twitter_data_file, indent=4, sort_keys=True)
        return

    def add_template(self,request):
        req = request.form
        name = req.get("name")
        # uuid= self.gen_uuid()
        # data = self.read_data()
        new = { "id": "", "parent": { }, "childs": {} }
        # data[uuid] = new
        self.write_tpl(name,new)
        return (1, "Object created")

    def add_template_field(self,request):
        req   = request.form
        tpl   = req.get("tpl")
        name  = req.get("name")
        value = req.get("value")
        print (id, tpl, name, value)
        data = self.read_tpl(tpl)
        data[name] = value
        self.write_tpl(tpl,data)
        return (1, "Field created", tpl)

    def del_template_field(self,tpl,field):
        data = self.read_tpl(tpl)
        if field in data:
            del data[field]
        self.write_tpl(tpl,data)
        return (1, "Field deleted", tpl)


    def get_template(self,tpl):
        c=0
        data = self.read_tpl(tpl)
        return data

    def add_object_field(self,request):
        req   = request.form
        id    = req.get("id")
        name  = req.get("name")
        value = req.get("value")
        # uuid= self.gen_uuid()
        print (id, name, value)
        data = self.read_data()
        data[id][name] = value
        self.write_data(data)
        return (1, "Object created", id)

    def api_get_first_id(self):
        data = self.read_data()
        for key in data:
            print ("id: ", key)
            return key

    def api_add_object(self,request):
        req = request.form
        name = req.get("name")
        fromtpl = req.get("fromtpl")
        uuid= self.gen_uuid()
        data = self.read_data()
        if req.get("fromtpl") == "on":
            tpl = req.get("tpl")
            tpl_data = self.read_tpl(tpl)
            new_json = { "id": uuid, "name": name }
            from jsonmerge import merge
            new = merge(new_json, tpl_data )
        else:
            new = { "id": uuid, "name": name, "childs": {}, "parent": {}, "type": "New" }
        data[uuid] = new
        # self.write_data(data)
        return json.dumps({"result": { "ok": "201", "uuid": uuid} }), 201
        # return (1, "Object created", uuid)

    def add_object(self,request):
        req = request.form
        name = req.get("name")
        fromtpl = req.get("fromtpl")
        uuid= self.gen_uuid()
        data = self.read_data()
        if req.get("fromtpl") == "on":
            tpl = req.get("tpl")
            tpl_data = self.read_tpl(tpl)
            new_json = { "id": uuid, "name": name }
            from jsonmerge import merge
            new = merge(new_json, tpl_data )
        else:
            new = { "id": uuid, "name": name, "childs": {}, "parent": {}, "type": "New" }
        data[uuid] = new
        self.write_data(data)
        return (1, "Object created", uuid)

    def del_parent(self,id,parent):
        data = self.read_data()
        del data[id]['parent'][parent]
        self.write_data(data)
        self.DisconnectParentChild(parent,id)
        return

    def api_del_parent(self,id,parent):
        data = self.read_data()
        del data[id]['parent'][parent]
        self.write_data(data)
        self.DisconnectParentChild(parent,id)
        return

    def DisconnectParentChild(self,parent,child):
        data = self.read_data()
        print ("data: ", data[parent] )
        del data[parent]['childs'][child]
        print ("data: ", data[parent] )
        self.write_data(data)
        return

    def del_child(self,id,child):
        data = self.read_data()
        del data[id]['childs'][child]
        self.write_data(data)
        self.DisconnectChildParent(child,id)
        return

    def DisconnectChildParent(self,parent,child):
        data = self.read_data()
        print ("data: ", data[parent] )
        del data[parent]['parent'][child]
        print ("data: ", data[parent] )
        self.write_data(data)
        return

    def del_object(self,id):
        data = self.read_data()
        del data[id]
        self.write_data(data)
        # with open("data.json", "w") as twitter_data_file:
        #     json.dump(data, twitter_data_file, indent=4, sort_keys=True)
        return

    def gen_uuid(self):
        return str(uuid.uuid1())

    def s_sign_in(self,request):
        if request.method == "POST":

            req = request.form

            username = req.get("username")
            password = req.get("password")
            if request.args.get('service'):
                service = req.args.get('service')
                print("S: " + service )

            if not username in self.users:
                print("Username not found")
                return ("r","sign_in")
            else:
                user = self.users[username]

            if not password == self.users[username]["password"]:
                print("Incorrect password")
                return ("r","sign_in")

            else:
                session["USERNAME"] = self.users[username]["username"]
                session["MAIL"] = self.users[username]["email"]
                print(session)
                print("session username set")
                return ("u","profile")

        return ("t","sign_in.html")

    def s_sign_out(self):
        session.pop("USERNAME", None)
        return ('r','sign_in')
